 ###############################################################################
 #  Copyright 2006 Ian McIntosh <ian@openanswers.org>
 #
 #  This program is free software; you can redistribute it and/or modify
 #  it under the terms of the GNU General Public License as published by
 #  the Free Software Foundation; either version 2 of the License, or
 #  (at your option) any later version.
 #
 #  This program is distributed in the hope that it will be useful,
 #  but WITHOUT ANY WARRANTY; without even the implied warranty of
 #  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 #  GNU Library General Public License for more details.
 #
 #  You should have received a copy of the GNU General Public License
 #  along with this program; if not, write to the Free Software
 #  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 ###############################################################################

require 'libglade2'
require 'delegate'

class GladeWindow < DelegateClass(Gtk::Window)
	def initialize(root_widget_name, options = {})
		options = {		# defaults
			:glade_file_name => sprintf("%s.glade", File.basename($0, '.rb'))		# script name
		}.merge(options)

		# load the widgets below 'root_widget' and auto-hookup all the methods
		glade = GladeXML.new(options[:glade_file_name], root_widget_name) { | handler_name | method(handler_name) }

		# create instance variables for each widget below us (except root widget, which is handled below)
		glade.widget_names.each { |name| instance_variable_set('@' + name, glade.get_widget(name)) unless name == root_widget_name }

		# in the class, we can refer to the GtkWindow as @window
		@window = glade.get_widget(root_widget_name)
		@window.realize

		@window.signal_connect('delete_event') { self.on_delete_event }

		super(@window)		# ...as required by delegation
	end

	def on_delete_event
		Gtk.main_quit		# overrideable
	end
end
