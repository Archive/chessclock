 ###############################################################################
 #  Copyright 2006 Ian McIntosh <ian@openanswers.org>
 #
 #  This program is free software; you can redistribute it and/or modify
 #  it under the terms of the GNU General Public License as published by
 #  the Free Software Foundation; either version 2 of the License, or
 #  (at your option) any later version.
 #
 #  This program is distributed in the hope that it will be useful,
 #  but WITHOUT ANY WARRANTY; without even the implied warranty of
 #  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 #  GNU Library General Public License for more details.
 #
 #  You should have received a copy of the GNU General Public License
 #  along with this program; if not, write to the Free Software
 #  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 ###############################################################################

require 'glade_window'

class ClockWindow < GladeWindow
	MS_PER_SECOND = 1000
	HIGHLIGHT_TIME = 10 * MS_PER_SECOND		# when a warning is displayed (in milliseconds)
	MS_PER_UPDATE = 100

	include Gdk::Keyval
	KEYVAL_PAUSE = GDK_space
	KEYVAL_END_GAME = GDK_Escape
	PLAYER_ONE_KEYS = [GDK_Shift_L, GDK_Control_L, GDK_Alt_L, GDK_Meta_L, GDK_Super_L, GDK_Hyper_L, GDK_Tab, GDK_Caps_Lock]
	PLAYER_TWO_KEYS = [GDK_Shift_R, GDK_Control_R, GDK_Alt_R, GDK_Meta_R, GDK_Super_R, GDK_Hyper_R, GDK_KP_Up, GDK_KP_Down, GDK_KP_Left, GDK_KP_Right, GDK_Up, GDK_Down, GDK_Left, GDK_Right, GDK_Return, GDK_BackSpace]

	def initialize
		super('clock_window')

		@player_labels = [@player1_label, @player2_label]
		@player_keys = [PLAYER_ONE_KEYS, PLAYER_TWO_KEYS]

		@is_new_game = true								# true only before the first clock starts
		@is_game_in_progress = false			# true when clocks are set up and neither is at 00:00.00 
		@is_paused = true
		@player_time_remaining = [0, 0]
		@player_increment = [0, 0]
		@use_bronstein = false
	end

	def set_player_time(player, time, increment)
		@player_time_remaining[player] = time.to_i * MS_PER_SECOND
		@player_increment[player] = increment.to_i * MS_PER_SECOND
	end

	def set_use_bronstein(rhs)
		@use_bronstein = rhs
	end

	def start(&proc)
		@end_proc = proc			# call this when user closes clock window

		@is_game_in_progress = true
		@is_new_game = true

		@player_with_initiative = PLAYER_TWO		# "black" hits timer to begin
		@num_turns = 0;
		@current_turn_time = 0

		set_pause(true)
		fullscreen
		show

		Gtk.timeout_add(MS_PER_UPDATE) { on_timeout }	# Note: GTimers aren't guaranteed to be accurate, but good enough
	end

private

	def set_pause(pause)
		@is_paused = pause
		update_labels
	end
	
	def on_timeout
		# Subtract time if game is active
		if @is_game_in_progress and not @is_paused
			if @player_time_remaining[@player_with_initiative] > 0
				@player_time_remaining[@player_with_initiative] -= MS_PER_UPDATE
				@current_turn_time += MS_PER_UPDATE
				@is_game_in_progress = @player_time_remaining[@player_with_initiative] > 0
			end
			update_label(@player_with_initiative)
		end
		return @is_game_in_progress		# return false (stopping timer) when game is over
	end

	def on_delete_event
		end_game
	end

	# Labels
	def update_labels
		@turn_label.markup = "<big><big>Turn #{@num_turns + 1}</big></big>"
		ALL_PLAYERS.each {|player| update_label(player) }
	end

	NUM_BIGS = 12
	def update_label(player)
		has_initiative = (@is_new_game == false and player == @player_with_initiative)
		highlight = @player_time_remaining[player] <= HIGHLIGHT_TIME

		@player_labels[player].markup = sprintf('%s%s%s%s%s%s%s',
			'<big>' * NUM_BIGS, (has_initiative) ? '<b>':'', (highlight) ? "<span foreground='red'>" : "",
			format_time(@player_time_remaining[player]),
			(highlight) ? "</span>" : "",	(has_initiative) ? '</b>':'',	'</big>' * NUM_BIGS)

		@player_labels[player].sensitive = !@is_paused
	end

	def format_time(time)
		minutes = time / (60 * MS_PER_SECOND)
		time -= minutes * (60 * MS_PER_SECOND)
		seconds = time.to_f / MS_PER_SECOND.to_f
		return sprintf('%02d:%04.1f', minutes, seconds)
	end

	# Key handlers
	def on_key_press(obj, event)
		if @is_game_in_progress
			ALL_PLAYERS.each {|player| change_initiative(other_player(player)) if @player_keys[player].include?(event.keyval) }
			set_pause(true) if event.keyval == KEYVAL_PAUSE
		end
		end_game if event.keyval == KEYVAL_END_GAME
	end

	def end_game
		@is_game_in_progress = false
		hide
		@end_proc.call
	end

	def change_initiative(player)
		if @player_with_initiative != player
			# Credit player with some bonus time for completing a turn
			if @use_bronstein
				# ..don't give more time than turn took
				@player_time_remaining[other_player(player)] += [@current_turn_time, @player_increment[other_player(player)]].min
			else
				@player_time_remaining[other_player(player)] += @player_increment[other_player(player)]
			end
			@current_turn_time = 0
			@num_turns += 1 if !@is_new_game and @player_who_hit_button_first == player
			@player_with_initiative = player
		end

		@player_who_hit_button_first = player if @is_new_game
		@is_new_game = false
		set_pause(false)				# either player can unpause a game

		update_labels
	end

	# Utils
	def other_player(p)
		return [PLAYER_TWO, PLAYER_ONE][p]
	end
end
