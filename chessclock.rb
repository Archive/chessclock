#!/usr/bin/ruby

 ###############################################################################
 #  Copyright 2006 Ian McIntosh <ian@openanswers.org>
 #
 #  This program is free software; you can redistribute it and/or modify
 #  it under the terms of the GNU General Public License as published by
 #  the Free Software Foundation; either version 2 of the License, or
 #  (at your option) any later version.
 #
 #  This program is distributed in the hope that it will be useful,
 #  but WITHOUT ANY WARRANTY; without even the implied warranty of
 #  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 #  GNU Library General Public License for more details.
 #
 #  You should have received a copy of the GNU General Public License
 #  along with this program; if not, write to the Free Software
 #  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 ###############################################################################

Dir.chdir(File.dirname(__FILE__))	# So that this file can be run from anywhere

###################################################################
# Constants
###################################################################
PLAYER_ONE, PLAYER_TWO = 0, 1
ALL_PLAYERS = [PLAYER_ONE, PLAYER_TWO]
APP_VERSION = 0.2

###################################################################
# Main
###################################################################
require 'addons_ruby'
require 'gtk2', 'libglade2', 'setup_window'

Gtk.init

$setup_window = SetupWindow.new
$setup_window.present

Gtk.main
