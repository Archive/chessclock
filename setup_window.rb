 ###############################################################################
 #  Copyright 2006 Ian McIntosh <ian@openanswers.org>
 #
 #  This program is free software; you can redistribute it and/or modify
 #  it under the terms of the GNU General Public License as published by
 #  the Free Software Foundation; either version 2 of the License, or
 #  (at your option) any later version.
 #
 #  This program is distributed in the hope that it will be useful,
 #  but WITHOUT ANY WARRANTY; without even the implied warranty of
 #  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 #  GNU Library General Public License for more details.
 #
 #  You should have received a copy of the GNU General Public License
 #  along with this program; if not, write to the Free Software
 #  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 ###############################################################################

require 'glade_window', 'clock_window'

class SetupWindow < GladeWindow
	UNIT_MINUTES, UNIT_SECONDS = 0, 1

	def initialize
		super('setup_window')
		@clock_window = ClockWindow.new
		@player_time_unit_combobox.active = UNIT_MINUTES
	end

private

	def on_setting_changed
		# Bronstein rule only makes sense with non-0 increment(s)
		@use_bronstein_checkbox.sensitive = (@player1_increment_spinbox.value > 0 or @player2_increment_spinbox.value > 0)
	end

	def on_begin_button_clicked
		# Send all settings to clock window
		multiplier = {UNIT_MINUTES => 60, UNIT_SECONDS => 1}[@player_time_unit_combobox.active]
		@clock_window.set_player_time(PLAYER_ONE, @player1_time_spinbox.value * multiplier, @player1_increment_spinbox.value)
		@clock_window.set_player_time(PLAYER_TWO, @player2_time_spinbox.value * multiplier, @player2_increment_spinbox.value)
		@clock_window.set_use_bronstein(@use_bronstein_checkbox.active?)

		# Start game
		hide
		@clock_window.start { present }		# when game is over, re-present setup window
	end
end
